//
//  ViewController.swift
//  MiraclePill
//
//  Created by macbook pro on 15/3/18.
//  Copyright © 2018 free developer hack. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var succesLabel: UIImageView!
    @IBOutlet weak var buyBttn: UIButton!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var cityEditText: UITextField!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var streetEditText: UITextField!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var fullNameEditText: UITextField!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var lineaSeparadora: UIView!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var pillLabel: UILabel!
    @IBOutlet weak var pillIcon: UIImageView!
    @IBOutlet weak var buyNowBttn: UIImageView!
    @IBOutlet weak var coutryEditText: UITextField!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var statePickerBttn: UIButton!
    @IBOutlet weak var statePiicker: UIPickerView!
    let states = ["Alaska","Arkansas","Alabama","California","Maine","New York"]
    override func viewDidLoad() {
        super.viewDidLoad()
        statePiicker.dataSource = self
        statePiicker.delegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    @IBAction func stateBttnPressed(_ sender: Any) {
        statePiicker.isHidden = false
        buyBttn.isHidden = true
        coutryEditText.isHidden = true
        countryLabel.isHidden = true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return states.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return states[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        statePickerBttn.setTitle(states[row], for: UIControlState.normal)
        statePiicker.isHidden = true
        buyBttn.isHidden = false
        coutryEditText.isHidden = false
        countryLabel.isHidden = false
        
    }
    @IBAction func pressBuying(_ sender: Any) {
        succesLabel.isHidden = false
        buyBttn.isHidden = true
        stateLabel.isHidden = true
        cityEditText.isHidden = true
        cityLabel.isHidden = true
        streetEditText.isHidden = true
        streetLabel.isHidden = true
        fullNameEditText.isHidden = true
        fullNameLabel.isHidden = true
        lineaSeparadora.isHidden = true
        moneyLabel.isHidden = true
        pillLabel.isHidden = true
        pillIcon.isHidden = true
        statePickerBttn.isHidden = true
        countryLabel.isHidden = true
        coutryEditText.isHidden = true
        countryLabel.isHidden = true
    }
}

